package com.goeuro.interview.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RouteResponse {

    @JsonProperty("dep_sid")
    private Integer departureId;
    @JsonProperty("arr_sid")
    private Integer arrivalId;
    @JsonProperty("direct_bus_route")
    private Boolean directBusRoute;

    public RouteResponse(final Integer departureId, final Integer arrivalId, final Boolean directBusRoute) {
        this.departureId = departureId;
        this.arrivalId = arrivalId;
        this.directBusRoute = directBusRoute;
    }

    public Integer getDepartureId() {
        return departureId;
    }

    public Integer getArrivalId() {
        return arrivalId;
    }

    public Boolean getDirectBusRoute() {
        return directBusRoute;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RouteResponse that = (RouteResponse) o;

        if (departureId != null ? !departureId.equals(that.departureId) : that.departureId != null) return false;
        if (arrivalId != null ? !arrivalId.equals(that.arrivalId) : that.arrivalId != null) return false;
        return directBusRoute != null ? directBusRoute.equals(that.directBusRoute) : that.directBusRoute == null;
    }

    @Override
    public int hashCode() {
        int result = departureId != null ? departureId.hashCode() : 0;
        result = 31 * result + (arrivalId != null ? arrivalId.hashCode() : 0);
        result = 31 * result + (directBusRoute != null ? directBusRoute.hashCode() : 0);
        return result;
    }
}
