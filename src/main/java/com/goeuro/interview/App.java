package com.goeuro.interview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableAutoConfiguration
public class App {

    private static String filepath;

    public static void main(final String[] args) {
        if(args.length != 1){
            die();
        }
        filepath = args[0];
        SpringApplication.run(App.class, args);
    }

    private static void die() {
        System.err.println("Please provide a filepath to bus routes.");
        System.exit(0);
    }

    @Bean
    public Config createConfig() {
        return new Config(filepath);
    }

}
