package com.goeuro.interview.service;

import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class BusRouteService {

    public boolean routeLookup(final Integer departureId, final Integer arrivalId, Map<Integer, List<Integer>> index ) {
        final List<Integer> departure = index.getOrDefault(departureId, Collections.emptyList());
        final List<Integer> arrival = index.getOrDefault(arrivalId, Collections.emptyList());
        return isListContainCommonNumber(departure, arrival);
    }

    private boolean isListContainCommonNumber(List<Integer> departure, List<Integer> arrival) {
        int i = 0, j = 0;
        while (i < departure.size() && j < arrival.size()) {
            if (departure.get(i) > arrival.get(j)) {
                j++;
            } else if (arrival.get(j) > departure.get(i)) {
                i++;
            } else {
                return true;
            }
        }
        return false;
    }
}
