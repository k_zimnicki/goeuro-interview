package com.goeuro.interview.service;

import com.goeuro.interview.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

@Service
public class IndexService {
    private static final String SEPARATOR = " ";

    @Autowired
    private Config config;

    private Map<Integer, List<Integer>> index = new HashMap<>();

    public Map<Integer, List<Integer>> getIndex(){
        return index;
    }

    @PostConstruct
    private void createIndex() {
        try (Stream<String> stream = Files.lines(Paths.get(config.getFilepath()))) {
            stream
                    .skip(1)
                    .map(line -> Arrays.asList(line.trim().split(SEPARATOR)))
                    .forEach(line -> {
                        Integer first = Integer.valueOf(line.get(0));
                        line.stream()
                                .skip(1)
                                .forEach(x -> {
                                    List<Integer> integers = index.getOrDefault(Integer.valueOf(x), new LinkedList<>());
                                    integers.add(first);
                                    index.put(Integer.valueOf(x), integers);
                        });
                    });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
