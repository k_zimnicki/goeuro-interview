package com.goeuro.interview;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import com.goeuro.interview.domain.RouteResponse;
import com.goeuro.interview.service.BusRouteService;
import com.goeuro.interview.service.IndexService;

@RestController
public class Api {

    @Autowired
    private IndexService indexService;

    @Autowired
    private BusRouteService busRouteService;

    @RequestMapping(value = "/api/direct", method = RequestMethod.GET)
    @ResponseBody
    public RouteResponse index(@RequestParam("dep_sid") final Integer departureId,
            @RequestParam("arr_sid") final Integer arrivalId) {
        Map<Integer, List<Integer>> index = indexService.getIndex();
        boolean isDirect = busRouteService.routeLookup(departureId, arrivalId, index);
        return new RouteResponse(departureId, arrivalId, isDirect);
    }

}
