package com.goeuro.interview;

public class Config {

    private final String filepath;

    public Config(String filepath) {
        this.filepath = filepath;
    }

    public String getFilepath() {
        return filepath;
    }

}
