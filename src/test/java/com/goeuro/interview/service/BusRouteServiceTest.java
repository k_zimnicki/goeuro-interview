package com.goeuro.interview.service;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

public class BusRouteServiceTest {
    @Test
    public void shouldReturnTrueForDirectRoute() throws Exception {
        Map<Integer, List<Integer>> index = ImmutableMap.<Integer, List<Integer>>builder()
                                                        .put(0, Lists.newArrayList(0, 2))
                                                        .put(1, Lists.newArrayList(0, 1)).build();

        boolean result = new BusRouteService().routeLookup(0, 1, index);

        assertTrue(result);
    }

    @Test
    public void shouldReturnFalseForNotDirectRoute() throws Exception {
        Map<Integer, List<Integer>> index = ImmutableMap.<Integer, List<Integer>>builder()
                                                        .put(0, Lists.newArrayList(0, 2))
                                                        .put(1, Lists.newArrayList(0, 1))
                                                        .put(2, Lists.newArrayList(3, 4)).build();

        boolean result = new BusRouteService().routeLookup(0, 2, index);

        assertFalse(result);
    }

    @Test
    public void shouldReturnFalseForNotExistingRoute() throws Exception {
        Map<Integer, List<Integer>> index = ImmutableMap.<Integer, List<Integer>>builder()
                                                        .put(0, Lists.newArrayList(0, 2))
                                                        .put(1, Lists.newArrayList(0, 1))
                                                        .put(2, Lists.newArrayList(3, 4)).build();

        boolean result = new BusRouteService().routeLookup(4, 7, index);

        assertFalse(result);
    }

}
