package com.goeuro.interview;

import org.springframework.context.annotation.Primary;

import org.springframework.stereotype.Component;

@Component
@Primary
public class ConfigMock extends Config {

    public ConfigMock() {
        super("");
    }

    public String getFilepath() {
        return getClass().getResource("/bus-routes-test.txt").getFile();
    }

}
