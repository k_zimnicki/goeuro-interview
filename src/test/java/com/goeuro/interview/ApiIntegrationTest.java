package com.goeuro.interview;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.goeuro.interview.domain.RouteResponse;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = App.class)
public class ApiIntegrationTest {

    @Autowired
    private Api api;

    @Test
    public void shouldReturnTrueForCorrectBusRoute() {
        int departureId = 1;
        int arrivalId = 6;

        RouteResponse result = api.index(departureId, arrivalId);

        assertEquals(result, new RouteResponse(departureId, arrivalId, true));
    }

    @Test
    public void shouldReturnFalseForWrongBusRoute() {
        int departureId = 0;
        int arrivalId = 5;

        RouteResponse result = api.index(departureId, arrivalId);

        assertEquals(result, new RouteResponse(departureId, arrivalId, false));
    }

    @Test
    public void shouldReturnFalseForNotExistBusRoute() {
        int departureId = 0;
        int arrivalId = 5;

        RouteResponse result = api.index(departureId, arrivalId);

        assertEquals(result, new RouteResponse(departureId, arrivalId, false));
    }
}
